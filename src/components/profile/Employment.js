import React from 'react'
import { Avatar, Box } from '@material-ui/core'
import { Check } from '../widgets'
import google from '../../images/google.png'
import './Employment.scss'

export default ({ employment: { employer, role, description, verified } }) => (
  <Box display="flex" flexDirection="row" className="employment" p={2}>
    {verified && <Check className="verified" />}
    <Box>
      <div className="employer-avatar">
        <Avatar src={google} className="extra-large"/>
      </div>
    </Box>
    <Box flexGrow={1} ml={3} mb={1}>
      <Box display="flex" flexDirection="column">
        <Box display="flex" flexDirection="row" mt={1}>
          <Box flexGrow={1}>
            <h3 className="secondary-color bold">{employer}</h3>
            <h5 className="grey-color">{role}</h5>
          </Box>
          <Box>
            <h3 className="secondary-color bold">Mar 2019 - Oct 2019</h3>
          </Box>
        </Box>
        <Box mt={2}>
          {description}
        </Box>
      </Box>
    </Box>
  </Box>
)