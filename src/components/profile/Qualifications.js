import React from 'react'
import { Card, CardContent } from '@material-ui/core'

export default () => (
  <div>
    <h2 className="secondary-color with-margin">Qualifications</h2>
    <Card square>
      <CardContent>
        No qualifications have been added
      </CardContent>
    </Card>
  </div>
)
