import React from 'react'
import { connect } from '@x/unify.react'
import { Avatar, Box, Card, CardContent, CardHeader, Chip, Grid, IconButton } from '@material-ui/core'
import { DialogButton, Check, Star } from '../widgets'
import AddReview from './AddReview'
import avatar from '../../images/avatar.png'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import './Reviews.scss'

export default connect('reviews')(
  ({ reviews }) => (
    <div>
      <h2 className="with-margin">Reviews</h2>
      {reviews.length === 0
        ? <p>No reviews have been added</p>
        : <Grid container direction="row" className="reviews">
            {reviews.map(({ reviewId, reviewer, rating, text, verified }) => (
              <Grid item xs={4} key={reviewId}>
                {verified && <Check className="verified" />}
                <Box mr={4} mb={4}>
                  <Card className="sans-serif" square>
                    <CardHeader
                      avatar={<Avatar src={avatar} />}
                      action={<IconButton><MoreVertIcon /></IconButton>}
                      title={
                        <div className="secondary-color">
                          <h3>{reviewer}</h3>
                          <h6>Recruiter</h6>
                        </div>
                      }
                    />
                    <CardContent>
                      <p>{text}</p>
                      <Box display="flex" justifyContent="space-between" alignItems="center" mt={3}>
                        <Chip variant="outlined" label={
                          <Box display="flex" flexDirection="row" alignItems="center">
                            <Star size={16} />
                            <Box ml={1}>{rating}</Box>
                          </Box>
                        } />
                        <Box display="flex" alignItems="center" className="secondary-color">
                          Show more
                          <KeyboardArrowDownIcon />
                        </Box>
                      </Box>
                    </CardContent>
                  </Card>
                </Box>
              </Grid>
            ))}
          </Grid>
      }
      <DialogButton action component={AddReview}>+</DialogButton>
    </div>
  )
)