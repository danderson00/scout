import React from 'react'
import { RowBox } from '../widgets'
import { Box, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import RatingBar from '../widgets/RatingBar'

export default ({ skill, onSkillRemoved }) => (
  <RowBox className="selected-skill" alignItems="center">
    <Box flexGrow={1}>
      <h3>{skill.name}</h3>
      <h5>Not rated</h5>
    </Box>

    <RatingBar />

    <IconButton size="small" onClick={() => onSkillRemoved(skill)}>
      <CloseIcon />
    </IconButton>
  </RowBox>
)
