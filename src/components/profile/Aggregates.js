import React from 'react'
import { Box } from '@material-ui/core'
import { Check, Star } from '../widgets'
import './Aggregates.scss'

export default ({ contractor: { rate, averageRating } }) => (
  <Box display="flex" flexDirection="row" className="aggregates" mt={4}>
    <Box flexGrow={1}>
      <p>Scout rate</p>
      <div>
        <span className="unit">$</span>
        {rate || <span className="grey-color">N/A</span> }
      </div>
    </Box>
    <Box flexGrow={1}>
      <p>Scout score</p>
      <div>
        <Star />
        {(averageRating || 0).toFixed(1)}
      </div>
    </Box>
    <Box flexGrow={1}>
      <p>Scout verified</p>
      <div>
        <Check />
        80<span className="unit">%</span>
      </div>
    </Box>
  </Box>
)