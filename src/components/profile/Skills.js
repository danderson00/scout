import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { Skills } from '../widgets'
import SelectedSkill from './SelectedSkill'
import { Card, CardContent } from '@material-ui/core'
import './Skills.scss'

export default publisher(connect('selectedSkills')(({ selectedSkills, publish }) => {
  const addSkill = ({ skillId }) => publish('skillSelected', { skillId })
  const removeSkill = ({ skillId }) => publish('skillSelected', { skillId, removed: true })

  return <div>
    <h2 className="secondary-color with-margin">Skills</h2>
    <Card square>
      <CardContent>
        <Skills
          resetScope
          selectedSkills={selectedSkills}
          onSkillSelected={addSkill}
          renderSkill={skill =>
            <SelectedSkill skill={skill} onSkillRemoved={removeSkill} />
          }
        />
      </CardContent>
    </Card>
  </div>
}))
