import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { v4 as uuid } from 'uuid'
import { Box, Chip, Tabs, Tab } from '@material-ui/core'
import { Menu } from '../widgets'
import Aggregates from './Aggregates'
import profileImage from '../../images/dwight.png'
import './Profile.scss'

export default connect('contractor')(
  publisher(({ contractor, publish }) => (
    <Box display="flex" flexDirection="column" className="profile profile-panel full-height dark-panel sans-serif">
      <Box display="flex" flexDirection="row" justifyContent="flex-end">
        <Menu items={[
          {
            text: 'Compare',
            handler: () => publish('compare', { comparisonId: uuid(), contractorId: contractor.contractorId })
          }
        ]} />
      </Box>

      <Box display="flex" alignItems="center" flexDirection="column" >
        <img src={profileImage} alt="Contractor" />
      </Box>

      <Box display="flex" alignItems="center" flexDirection="column" mt={4}>
        <h2 className="serif with-margin">{contractor.name}</h2>
        <h3>{contractor.role}</h3>
      </Box>

      <Aggregates contractor={contractor} />

      <Box display="flex" flexDirection="row" justifyContent="space-between" my={4}>
        <Chip variant="outlined" label="Available now" />
        <Chip variant="outlined" label="Full-time" />
        <Chip variant="outlined" label="Remote" />
        <Chip variant="outlined" label="ACT" />
      </Box>

      <Tabs indicatorColor="secondary" value={0}>
        <Tab label="Overview" />
        <Tab label="Availability" disabled />
      </Tabs>
      <Box mt={4}>
        {contractor.description}
      </Box>
    </Box>
  ))
)
