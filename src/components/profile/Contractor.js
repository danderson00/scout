import React from 'react'
import { Box, Grid } from '@material-ui/core'
import { Profile, Reviews, EmploymentHistory, Projects, Skills } from '.'
import Layout from '../Layout'
import SearchPanel from '../search/SearchPanel'
import './panels.scss'
import { RowBox } from '../widgets'
import Qualifications from './Qualifications'

export default () => (
  <Layout activeItem="Scout">
    <Grid container direction="row" className="contractor">
      <Grid item xs={4}>
        <Profile />
      </Grid>
      <Grid item xs={8}>
        <div className="profile-right-top-panel">
          <SearchPanel />
          <div className="people-panel">
            <h1 className="with-margin">People</h1>
            <Reviews />
          </div>
        </div>
        <div className="profile-right-secondary-panel">
          <EmploymentHistory />
          <Projects />

          <RowBox>
            <Box flexGrow={1} mr={2}>
              <Skills />
            </Box>
            <Box flexGrow={2 /* ??? */} ml={2}>
              <Qualifications />
            </Box>
          </RowBox>
        </div>
      </Grid>
    </Grid>
  </Layout>
)
