import React from 'react'
import { connect } from '@x/unify.react'
import { Box, Card } from '@material-ui/core'
import { DialogButton } from '../widgets'
import AddEmployment from './AddEmployment'
import Employment from './Employment'

export default connect('employment')(
  ({ employment }) => (
    <div>
      <h2 className="secondary-color with-margin">Employment History</h2>
      {employment.length === 0
        ? <p>No employment history has been added</p>
        : <Card square>
            <Box p={2}>
              {employment.map(employment => (
                <Box display="flex" flexDirection="column" key={employment.employmentId}>
                  <Box flexGrow={1}>
                    <Employment employment={employment} />
                  </Box>
                </Box>
              ))}
            </Box>
          </Card>
      }
      <DialogButton action component={AddEmployment}>+</DialogButton>
    </div>
  )
)