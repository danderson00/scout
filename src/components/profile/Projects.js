import React from 'react'
import { connect } from '@x/unify.react'
import { Box, Card, CardContent, CardHeader, Grid, IconButton } from '@material-ui/core'
import { DialogButton, Check } from '../widgets'
import AddProject from './AddProject'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import './Projects.scss'

export default connect('projects')(
  ({ projects }) => (
    <div>
      <h2 className="secondary-color with-margin">Projects</h2>
      {projects.length === 0
        ? <p>No projects have been added</p>
        : <Grid container direction="row">
            {projects.map(({ projectId, company, title, text, verified }) => (
              <Grid item xs={4} key={projectId}>
                <Box mr={4} className="project">
                  {verified && <Check className="verified" />}
                  <Card square className="project-panel">
                    <CardHeader
                      action={<IconButton><MoreVertIcon /></IconButton>}
                      title={
                        <h3 className="tertiary-color">{company}</h3>
                      }
                    />
                    <CardContent>
                      <Box mt={-4}>
                        <h2 className="secondary-color">{title}</h2>
                      </Box>
                      <Box mt={1}>
                        <p>{text}</p>
                      </Box>
                    </CardContent>
                  </Card>
                </Box>
              </Grid>
            ))}
          </Grid>
      }
      <DialogButton action component={AddProject}>+</DialogButton>
    </div>
  )
)