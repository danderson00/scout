import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit, Checkbox } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function AddEmployment({ publish, open, onClose }) {
    const add = employment => (
      publish('employment', { employmentId: uuid(), ...employment }).then(onClose)
    )

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit margin="none">
          <DialogTitle>Add Employment</DialogTitle>
          <DialogContent>
            <Text name="employer" label="Employer" required autoFocus />
            <Text name="role" label="Role" required />
            <Text name="description" label="Description" multiline rows={3} />
            <Checkbox name="verified" label="Verified" />
          </DialogContent>
          <DialogActions>
            <Submit>Add Employment</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)