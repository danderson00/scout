import React from 'react'
import { auth, publisher } from '@x/unify.react'
import { Form, Text, Submit, Radio, Checkbox } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default auth(publisher(
  function AddReview({ auth, publish, open, onClose }) {
    const add = review => (
      publish('review', {
        reviewId: uuid(),
        reviewer: auth.user.data.fullname,
        ...review
      }).then(onClose)
    )

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit margin="none">
          <DialogTitle>Add Review</DialogTitle>
          <DialogContent>
            <Radio name="rating" label="Rating" values={[1, 2, 3, 4, 5]} row />
            <Text name="text" label="Review text" multiline rows={3} fullWidth />
            <Checkbox name="verified" label="Verified" />
          </DialogContent>
          <DialogActions>
            <Submit>Add Review</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
))
