import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit, Checkbox } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function AddProject({ publish, open, onClose }) {
    const add = project => (
      publish('project', { projectId: uuid(), ...project }).then(onClose)
    )

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit margin="none">
          <DialogTitle>Add Project</DialogTitle>
          <DialogContent>
            <Text name="company" label="Company" required autoFocus />
            <Text name="title" label="Title" required />
            <Text name="text" label="Project text" multiline rows={3} />
            <Checkbox name="verified" label="Verified" />
          </DialogContent>
          <DialogActions>
            <Submit>Add Project</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)