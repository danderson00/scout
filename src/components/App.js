import React from 'react'
import { Navigator } from '@x/unify.react'
import navigation from '../navigation'
import '../styles'

export default () => (
  <Navigator
    machine={navigation}
    connect="user"
    resolveComponent={path => {
      return import(/* webpackPrefetch: true */ `./${path.join('/')}`).then(m => m.default)
    }}
  />
)
