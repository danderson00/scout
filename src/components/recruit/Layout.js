import React from 'react'
import { ColumnBox } from '../widgets'
import Layout from '../Layout'
import SearchPanel from '../search/SearchPanel'
import './Layout.scss'

export default ({ children }) => (
  <Layout activeItem="Recruit">
    <ColumnBox className="recruit full-height">
      <SearchPanel/>

      <ColumnBox flexGrow={1} className="recruit-panel">
        {children}
      </ColumnBox>
    </ColumnBox>
  </Layout>
)
