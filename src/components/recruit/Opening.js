import React, { useState } from 'react'
import { connect, publisher, useEvaluate } from '@x/unify.react'
import { Box } from '@material-ui/core'
import { DragDropContext, Droppable } from 'react-beautiful-dnd'
import { ColumnBox, RowBox } from '../widgets'
import Layout from './Layout'
import { Header, RightPanel, CandidateList } from '.'
import './Opening.scss'

export default publisher(connect('opening', 'selectedContractors', 'selectedSkills')(
  ({ opening, selectedContractors, selectedSkills, openingId, publish, synced }) => {
    // as react-beautiful-dnd expects synchronous list updates, hack them in "optimistically"
    // this needs a better, generic long term solution
    const [lastDropped, setLastDropped] = useState({})
    const [draggingContractorId, setDraggingContractorId] = useState()

    const selectedContractorIds = selectedContractors.map(x => x.contractorId)
    const statuses = selectedContractors.reduce(
      (groups, listed) => ({
        ...groups,
        [listed.status]: listed.contractorId === lastDropped.contractorId
          ? groups[listed.status]
          : [...groups[listed.status], listed.contractorId]
      }),
      { shortlisted: [], interviews: [], offers: [], notSuitable: [] }
    )

    if(lastDropped.contractorId && lastDropped.destination !== 'search') {
      statuses[lastDropped.destination].push(lastDropped.contractorId)
    }

    const [contractors] = useEvaluate('matchingContractors', {}, synced && [selectedSkills.map(x => x.skillId)])

    const availableContractors = contractors.filter(({ contractorId }) =>
      (lastDropped.destination === 'search' || lastDropped.contractorId !== contractorId) &&
      !selectedContractorIds.includes(contractorId)
    )

    const handleDrag = ({ draggableId }) => setDraggingContractorId(draggableId)

    const handleDrop = ({ draggableId, destination }) => {
      if(destination && destination.droppableId) {
        publish('contractorListed', {
          contractorId: draggableId,
          status: destination.droppableId === 'search' ? undefined : destination.droppableId
        })
        setLastDropped({ contractorId: draggableId, destination: destination.droppableId })
      }
      setDraggingContractorId(undefined)
    }

    return (
      <Layout>
        <DragDropContext onDragEnd={handleDrop} onBeforeCapture={handleDrag}>
          <RowBox flexGrow={1}>
            <ColumnBox flexGrow={1} className="recruit-opening-panel">
              <Header
                opening={opening}
                contractors={availableContractors}
                selectedContractors={statuses}
                selectedSkills={selectedSkills}
              />

              <RowBox mt={4} className="candidates">
                <Droppable droppableId="search">
                  {({ innerRef, droppableProps, placeholder }) => (
                    <Box ref={innerRef} flexGrow={1} mb={2} {...droppableProps}>
                      <CandidateList
                        contractors={availableContractors}
                        draggingContractorId={draggingContractorId}
                        openingId={openingId}
                      />

                      <div style={{ display: availableContractors.length === 0 ? 'none' : 'block' }}>
                        {placeholder}
                      </div>
                    </Box>
                  )}
                </Droppable>
              </RowBox>
            </ColumnBox>

            <RightPanel selectedContractors={statuses} openingId={openingId} />
          </RowBox>
        </DragDropContext>
      </Layout>
    )
  }
))
