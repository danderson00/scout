import React from 'react'
import { ColumnBox } from '../widgets'
import { ConnectedSelectedContractor as SelectedContractor } from './SelectedContractor'
import { Draggable, Droppable } from 'react-beautiful-dnd'
import { DropTarget } from '../widgets'
import './TargetList.scss'

export default ({ status, displayStatus, selectedContractors, openingId }) => {
  return (
    <Droppable droppableId={status}>
      {({ innerRef, droppableProps, placeholder }) => (
        <div ref={innerRef} {...droppableProps} className="target-list">
          <h3>{displayStatus}</h3>
          <ColumnBox>
            {selectedContractors.map((contractorId, index) => (
              <Draggable key={contractorId} draggableId={contractorId} index={index}>
                {({ innerRef, draggableProps, dragHandleProps }) => (
                  <div ref={innerRef} {...draggableProps} {...dragHandleProps}>
                    <SelectedContractor resetScope={{ contractorId }} openingId={openingId} />
                  </div>
                )}
              </Draggable>
            ))}

            {selectedContractors.length === 0 && <DropTarget />}
            {/* hide the placeholder if target has no items - no built in support */}
            <div style={{ display: selectedContractors.length === 0 ? 'none' : 'block' }}>
              {placeholder}
            </div>
          </ColumnBox>
        </div>
      )}
    </Droppable>
  )
}
