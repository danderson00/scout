import React from 'react'
import { Box, Chip, Input, InputAdornment, Tab, Tabs } from '@material-ui/core'
import { RowBox } from '../widgets'
import SearchIcon from '@material-ui/icons/Search'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'

export default ({ opening, contractors, selectedContractors, selectedSkills }) => <>
  <Box mb={2}>
    <h1>{opening.role}</h1>
    <h2>(Job ID: {opening.identifier})</h2>
  </Box>

  <RowBox>
    {selectedSkills.map(({ skillId, name }) =>
      <Chip key={skillId} color="secondary" label={name} style={{ marginRight: 4 }} />
    )}
  </RowBox>

  <RowBox>
    <Box flexGrow={1}>
      <Tabs indicatorColor="secondary" value={0}>
        <Tab label={`All applicants (${contractors.length})`} />
        <Tab label={`Shortlist (${selectedContractors.shortlisted.length})`} />
        <Tab label={`Interviews (${selectedContractors.interviews.length})`} />
        <Tab label={`Offers (${selectedContractors.offers.length})`} />
        <Tab label={`Not suitable (${selectedContractors.notSuitable.length})`} />
      </Tabs>
    </Box>
    <Box>
      {/*<button>+</button>*/}
    </Box>
  </RowBox>

  <RowBox mt={6}>
    <Box flexGrow={1}>
      <Input
        variant="outlined"
        fullWidth
        placeholder="Search by name..."
        endAdornment={
          <InputAdornment position="end">
            <SearchIcon />
          </InputAdornment>
        }
      />
    </Box>

    <RowBox alignItems="center" ml={10} className="sort">
      <span>Sort by: <b>Scout score</b></span>
      <KeyboardArrowDownIcon />
    </RowBox>
  </RowBox>
</>