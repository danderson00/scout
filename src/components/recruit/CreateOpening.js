import React, { useState } from 'react'
import { connect, publisher } from '@x/unify.react'
import { Form, Submit, Text } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Chip } from '@material-ui/core'
import Layout from './Layout'
import { Skills } from '../widgets'
import './CreateOpening.scss'

export default connect('user')(publisher(({ user: { recruiterId, recruiter = {} }, publish }) => {
  const [selectedSkills, setSelectedSkills] = useState([])
  const add = opening => {
    const openingId = uuid()
    return publish('opening', { openingId, recruiterId, opening })
      .then(() => Promise.all(selectedSkills.map(({ skillId }) =>
        publish('skillSelected', { openingId, skillId })
      )))
  }

  return (
    <Layout>
      <h2>Create New Opening</h2>
      <Form onSubmit={add} resetOnSubmit margin="none" className="create-opening">
        <Text name="employer" label="Employer" value={recruiter.employer} disabled /> {/* this should be moved to an aspect */}
        <Text name="identifier" label="Identifier" required autoFocus />
        <Text name="role" label="Role" required />
        <Text name="rate" label="Rate" required numeric />
        <Text name="startDate" label="Starts" required />
        <Text name="duration" label="Duration" required />
        <Text name="description" label="Description" multiline rows={3} />

        <Skills
          selectedSkills={selectedSkills}
          onSkillSelected={skill => setSelectedSkills([...selectedSkills, skill])}
          renderSkill={({ skillId, name }) => (
            <Chip
              color="secondary"
              label={name}
              onDelete={() => setSelectedSkills(selectedSkills.filter(x => x.skillId !== skillId))}
            />
          )}
        />

        <Submit variant="contained">Add Opening</Submit>
      </Form>
    </Layout>
  )
}))
