import React from 'react'
import Layout from './Layout'
import Openings from '../dashboard/Openings'
import './Recruit.scss'

export default () => (
  <Layout>
    <h2>Current Openings</h2>
    <Openings />
  </Layout>
)
