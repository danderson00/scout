import React from 'react'
import { ColumnBox } from '../widgets'
import { TargetList } from '.'
import './RightPanel.scss'

export default ({ selectedContractors: { shortlisted, interviews, offers, notSuitable }, openingId }) => (
  <ColumnBox className="right-panel">
    <h2>Create your lists</h2>
    <p>Drag and drop Scouts to your list</p>
    <TargetList status="shortlisted" displayStatus="Shortlist" selectedContractors={shortlisted} openingId={openingId} />
    <TargetList status="interviews" displayStatus="Interviews" selectedContractors={interviews} openingId={openingId} />
    <TargetList status="offers" displayStatus="Offers" selectedContractors={offers} openingId={openingId} />
    <TargetList status="notSuitable" displayStatus="Not suitable" selectedContractors={notSuitable} openingId={openingId} />
  </ColumnBox>
)