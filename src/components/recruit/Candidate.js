import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { Box, Button, Card, Chip } from '@material-ui/core'
import { ColumnBox, RowBox } from '../widgets'
import Aggregates from '../profile/Aggregates'
import avatar from '../../images/recruit-avatar.png'
import dragHandle from '../../images/drag-handle.png'
import './Candidate.scss'

export default publisher(connect('contractor')(({ contractor, selected, onClick, publish }) => {
  const { contractorId, name, role } = contractor

  return (
    <Card
      square
      elevation={0}
      className={`clickable candidate${selected ? ' selected' : ''}`}
      onClick={onClick}
    >
      <RowBox alignItems="center" justifyContent="center" m={4}>
        <Box>
          <img src={dragHandle} alt="drag" />
        </Box>

        <Box px={4}>
          <img src={avatar} alt="candidate"/>
        </Box>

        <ColumnBox flexGrow={1}>
          <RowBox>
            <ColumnBox flexGrow={1}>
              <h2 className="serif">{name}</h2>
              <h3>{role}</h3>
            </ColumnBox>

            <Aggregates contractor={contractor}/>
          </RowBox>

          <RowBox justifyContent="space-between" mt={4}>
            <Box display="flex" flexGrow={1} className="chips">
              <Chip variant="outlined" label="Available now" />
              <Chip variant="outlined" label="ACT" />
              <Chip variant="outlined" label="Baseline clearance" />
            </Box>
            <Box pr={3}>
              <Button
                variant="outlined"
                onClick={stopPropagation(
                  () => publish('contractorSelected', { contractorId })
                )}
              >
                View details
              </Button>
            </Box>
          </RowBox>
        </ColumnBox>
      </RowBox>
    </Card>
  )
}))

// if there are elements with click handlers underneath the menu, this stops the event propagating
const stopPropagation = handler => e => {
  e && e.stopPropagation()
  handler(e)
}
