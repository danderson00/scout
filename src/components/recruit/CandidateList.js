import React from 'react'
import { Draggable } from 'react-beautiful-dnd'
import { DropTarget } from '../widgets'
import { Candidate, ConnectedSelectedContractor as SelectedContractor } from '.'

export default ({ contractors, draggingContractorId, openingId }) => {
  return <>
    {contractors.map(({ contractorId }, index) => (
      <Draggable key={contractorId} draggableId={contractorId} index={index}>
        {({ innerRef, draggableProps, dragHandleProps }, { isDragging }) => (
          <div
            style={{ display: 'inline-flex' }}
            ref={innerRef}
            {...{
              ...draggableProps,
              style: {
                ...draggableProps.style,
                width: isDragging ? 450 : draggableProps.style.width
              }
            }}
            {...dragHandleProps}
          >
            {draggingContractorId === contractorId
              ? <SelectedContractor resetScope={{ contractorId }} openingId={openingId} />
              : <Candidate resetScope={{ contractorId }} />
            }
          </div>
        )}
      </Draggable>
    ))}
    {contractors.length === 0 && <DropTarget />}
  </>
}
