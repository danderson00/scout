import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { ColumnBox, RowBox } from '../widgets'
import { Box, IconButton } from '@material-ui/core'
import CloseIcon from '@material-ui/icons/Close'
import dragHandle from '../../images/drag-handle.png'
import avatar from '../../images/recruit-avatar.png'
import './SelectedContractor.scss'

const SelectedContractor = publisher(({ contractor, openingId, publish }) => {
  const { contractorId, name, role, rate } = contractor

  const unselect = () => {
    return publish('contractorListed', { contractorId, openingId })
  }

  return (
    <RowBox className="selected-contractor" alignItems="center">
      <img src={dragHandle} alt="drag" className="drag-icon" />

      <Box px={4}>
        <img src={avatar} alt="candidate" className="avatar"/>
      </Box>

      <ColumnBox flexGrow={1}>
        <h3 className="serif">{name}</h3>
        <h5 className="grey">{role}</h5>
      </ColumnBox>

      <Box mr={2}>
        ${rate} / day
      </Box>

      <IconButton size="small" onClick={unselect}>
        <CloseIcon />
      </IconButton>
    </RowBox>
  )
})

export default SelectedContractor
export const ConnectedSelectedContractor = connect('contractor')(SelectedContractor)
