import React from 'react'
import { publisher } from '@x/unify.react'
import { Box, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { ColumnBox, Logo, RedDot } from './widgets'
import './Sidebar.scss'

import dashboard from '../images/icon-dashboard.png'
import recruit from '../images/icon-recruit.png'
import scout from '../images/icon-scout.png'
import messages from '../images/icon-messages.png'
import learn from '../images/icon-learn.png'

export default publisher(({ publish, activeItem }) => {
  const navigationItems = [
    { image: dashboard, text: 'Dashboard', topic: 'dashboard' },
    { image: recruit, text: 'Recruit', topic: 'recruit' },
    { image: scout, text: 'Scout', topic: 'scout' },
    { image: messages, text: 'Messages' },
    { image: learn, text: 'Learn' }
  ]

  const navigationItem = ({ image, text, topic }) => (
    <Box key={text} mb={2}>
      <ListItem
        className={text === activeItem ? 'active' : ''}
        onClick={() => topic && publish(topic)}
        button
      >
        <ListItemIcon>
          {text === activeItem
            ? <RedDot />
            : <img src={image} alt={text} />
          }
        </ListItemIcon>
        <ListItemText primary={text} className="grey-color" />
      </ListItem>
    </Box>
  )

  return (
    <ColumnBox className="sidebar" p={8}>
      <Box mt={2} mb={10}>
        <Logo small black />
      </Box>

      <List>
        {navigationItems.map(navigationItem)}
      </List>
    </ColumnBox>
  )
})
