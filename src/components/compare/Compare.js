import React from 'react'
import { Box } from '@material-ui/core'
import { ColumnBox, RowBox } from '../widgets'
import SearchPanel from '../search/SearchPanel'
import './Compare.scss'

export default () => (
  <ColumnBox className="compare full-height">
    <SearchPanel />

    <RowBox flexGrow={1}>
      <Box flexGrow={1} className="compare-panel">
        <h1>Compare</h1>
      </Box>
    </RowBox>
  </ColumnBox>
)