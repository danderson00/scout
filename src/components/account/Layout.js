import React from 'react'
import { Box } from '@material-ui/core'
import { Logo } from '../widgets'
import './Layout.scss'

export default ({ children }) => (
  <Box display="flex" flexDirection="row" className="account full-height">
    <Box flexGrow={0.3} display="flex" flexDirection="column" alignItems="center" justifyContent="center">
      <Logo />
      <main>{children}</main>
    </Box>
    <Box flexGrow={1}>
    </Box>
  </Box>
)
