import React, { useState } from 'react'
import { auth, publisher } from '@x/unify.react'
import { Box } from '@material-ui/core'
import { Checkbox, Form, Submit, Text } from '@danderson00/react-forms-material'
import Layout from './Layout'
import './Login.scss'

export default auth(publisher(({ auth, publish }) => {
  const [creating, setCreating] = useState(false)

  if(auth.authenticated) {
    setTimeout(() => publish('loggedIn'), 1000)
  }

  const login = ({ username, password, data, persistent }) => {
    return (creating
      ? auth.createUser({ username, password, data, provider: 'password' }, persistent)
      : auth.authenticate({ username, password, provider: 'password' }, persistent))
        .then(result => {
          if(result.success) {
            publish('loggedIn')
          }
          return result
        })
  }

  return (
    <Layout>
      <Form className="login" onSubmit={login} margin="dense">
        <p>Username</p>
        <Text name="username" required autoFocus fullWidth />

        <p>Password</p>
        <Text name="password" type="password" required fullWidth />

        <Submit fullWidth>
          {creating ? 'Create account' : 'Sign in'}
        </Submit>

        <Box display="flex" flexDirection="row" alignItems="center" justifyContent="space-between" style={{ width: '100%' }}>
          <Box display="flex" flexDirection="row" alignItems="center">
            <Checkbox name="persistent" checked={true} />
            <label>Remember me</label>
          </Box>
          <Box>
            <button
              className="link"
              onClick={() => setCreating(!creating)}
            >
              {creating ? 'Sign in' : 'Create account'}
            </button>
          </Box>
          <Box>
            <a href="forgotten-password">Forgot password?</a>
          </Box>
        </Box>
      </Form>
    </Layout>
  )
}))
