import React from 'react'
import { publisher } from '@x/unify.react'
import Layout from '../account/Layout'
import { Form, Submit, Text } from '@danderson00/react-forms-material'

export default publisher(({ publish }) => {
  const submit = contractor => publish('user.contractor')
    .then(({ contractorId }) => publish('contractor', { contractorId, contractor }))

  return (
    <Layout>
      <Form onSubmit={submit}>
        <p>Full name</p>
        <Text name="name" required fullWidth />

        <p>Job title</p>
        <Text name="role" fullWidth />

        <p>Employer</p>
        <Text name="employer" fullWidth />

        <p>Rate</p>
        <Text name="rate" fullWidth numeric />

        <Submit fullWidth>Save</Submit>
      </Form>
    </Layout>
  )
})
