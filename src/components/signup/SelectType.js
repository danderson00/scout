import React from 'react'
import { publisher } from '@x/unify.react'
import { Box, Button } from '@material-ui/core'
import Layout from '../account/Layout'

export default publisher(({ publish }) => {
  const selectType = type => publish('user.type', { type })

  return (
    <Layout>
      <Box
        className="full-width button-panel"
        flexGrow={1}
        display="flex"
        flexDirection="column"
        alignItems="center"
      >
        <h3>I am a:</h3>
        <Button fullWidth onClick={() => selectType('contractor')}>Contractor</Button>
        <Button fullWidth onClick={() => selectType('recruiter')}>Recruiter</Button>
      </Box>
    </Layout>
  )
})
