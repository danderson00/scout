import React from 'react'
import { publisher } from '@x/unify.react'
import Layout from '../account/Layout'
import { Form, Submit, Text } from '@danderson00/react-forms-material'

export default publisher(({ publish }) => {
  const submit = recruiter => publish('user.recruiter')
    .then(({ recruiterId }) => publish('recruiter', { recruiterId, recruiter }))

  return (
    <Layout>
      <Form onSubmit={submit}>
        <p>Full name</p>
        <Text name="fullName" required fullWidth />

        <p>Job title</p>
        <Text name="role" fullWidth />

        <p>Employer</p>
        <Text name="employer" fullWidth />

        <Submit fullWidth>Save</Submit>
      </Form>
    </Layout>
  )
})
