import React from 'react'
import { publisher } from '@x/unify.react'
import { Form, Text, Submit } from '@danderson00/react-forms-material'
import { v4 as uuid } from 'uuid'
import { Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@material-ui/core'

export default publisher(
  function AddContractor({ publish, open, onClose }) {
    const add = contractor => (
      publish('contractor', { contractorId: uuid(), contractor }).then(onClose)
    )

    return (
      <Dialog open={open} onClose={onClose}>
        <Form onSubmit={add} resetOnSubmit margin="none">
          <DialogTitle>Add Contractor</DialogTitle>
          <DialogContent>
            <Text name="name" label="Name" required autoFocus />
            <Text name="role" label="Role" required />
            <Text name="rate" label="Rate" required numeric />
            <Text name="description" label="Description" multiline rows={3} />
          </DialogContent>
          <DialogActions>
            <Submit>Add Contractor</Submit>
            <Button onClick={onClose}>Cancel</Button>
          </DialogActions>
        </Form>
      </Dialog>
    )
  }
)