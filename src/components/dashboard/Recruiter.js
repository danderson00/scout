import React from 'react'
import { connect } from '@x/unify.react'
// import { Box } from '@material-ui/core'
// import { ColumnBox, RowBox } from '../widgets'
import Openings from './Openings'

export default connect('recruiter')(({ recruiter }) => <>
  <h1>Welcome, {recruiter.fullName}</h1>
  <h2>Your Openings</h2>
  <Openings allowAdd />
</>)
