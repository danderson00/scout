import React from 'react'
import { connect } from '@x/unify.react'
import Contractor from './Contractor'
import Recruiter from './Recruiter'
import Layout from '../Layout'
import { ColumnBox } from '../widgets'
import SearchPanel from '../search/SearchPanel'
import { Box } from '@material-ui/core'
import './Dashboard.scss'

export default connect('user')(({ user: { type, contractorId, recruiterId } }) => {
  const resolveComponent = () => {
    switch(type) {
      case 'contractor': return <Contractor scope={{ contractorId }} />
      case 'recruiter': return <Recruiter scope={{ recruiterId }} />
      default: return null
    }
  }

  return (
    <Layout activeItem="Dashboard">
      <ColumnBox className="dashboard full-height">
        <SearchPanel />
        <Box flexGrow={1} className="dashboard-panel">
          {resolveComponent()}
        </Box>
      </ColumnBox>
    </Layout>
  )
})
