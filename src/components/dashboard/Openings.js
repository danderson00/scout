import React from 'react'
import { connect, publisher } from '@x/unify.react'
import { RowBox } from '../widgets'
import { Card, CardContent, IconButton } from '@material-ui/core'
import AddIcon from '@material-ui/icons/AddCircle'
import './Openings.scss'

export default publisher(connect('openings')(({ openings, allowAdd, publish }) => (
  <RowBox alignItems="center">
    {allowAdd && <div>
      <IconButton onClick={() => publish('createOpening')}>
        <AddIcon style={{ fontSize: 48 }} color="secondary" />
      </IconButton>
    </div>}

    {openings.map(({ openingId, role, identifier, rate, startDate, duration }) =>
      <Card
        square
        key={openingId}
        onClick={() => publish('openingSelected', { openingId })}
        className="opening"
      >
        <CardContent>
          <h2>{role}</h2>
          <h3>(ID: {identifier})</h3>
          <p>Rate: ${rate}</p>
          <p>Starts: {startDate}</p>
          <p>Duration: {duration}</p>
        </CardContent>
      </Card>
    )}
  </RowBox>
)))
