import React from 'react'
import { Box } from '@material-ui/core'
import { RowBox } from './widgets'
import Sidebar from './Sidebar'

export default ({ children, activeItem }) => (
  <RowBox>
    <Box style={{ width: 300 }}>
      <Sidebar activeItem={activeItem} />
    </Box>

    <Box flexGrow={1}>
      {children}
    </Box>
  </RowBox>
)
