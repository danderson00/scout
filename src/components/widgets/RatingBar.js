import React from 'react'

export default ({ rating }) => {
  const style = {
    width: 200,
    height: 8,
    borderRadius: 4,
    background: '#E7E7E7'
  }

  return <div style={style}></div>
}
