import React from 'react'
import { Box } from '@material-ui/core'

export const RowBox = props => (
  <Box
    display="flex"
    flexDirection="row"
    {...props}
    className={`row-box ${props.className || ''}`}
  />
)

export const ColumnBox = props => (
  <Box
    display="flex"
    flexDirection="column"
    className={`column-box ${props.className || ''}`}
    {...props}
  />
)
