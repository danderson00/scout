import React, { createElement, useState } from 'react'
import { Button, Fab, IconButton } from '@material-ui/core'

export default function DialogButton({ children, component, variant, fullWidth, action, icon, startIcon, ...props }) {
  const [open, setOpen] = useState(false)

  return <>
    {action
      ? <Fab onClick={() => setOpen(true)} className="action">
          {children}
        </Fab>
      : icon
        ? <IconButton onClick={() => setOpen(true)}>
            {children}
          </IconButton>
        : <Button
            variant={variant || 'contained'}
            fullWidth={fullWidth}
            onClick={() => setOpen(true)}
            startIcon={startIcon}
          >
            {children}
          </Button>
    }
    {createElement(component, {
      ...props,
      open,
      onClose: () => setOpen(false)
    })}
  </>
}
