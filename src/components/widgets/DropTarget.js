import React from 'react'
import { Box } from '@material-ui/core'

export default () => (
  <Box
    display="flex"
    justifyContent="center"
    alignItems="center"
    style={{
      height: 80,
      border: '2px dashed #1B4650',
      background: '#1B465026',
      borderRadius: 12,
      fontWeight: 'bold'
    }}>
    Drag to add
  </Box>
)