import React, { Fragment } from 'react'
import { ColumnBox } from '.'
import Autocomplete, { createFilterOptions } from '@material-ui/lab/Autocomplete'
import { TextField } from '@material-ui/core'
import { connect, publisher } from '@x/unify.react'
import { v4 as uuid } from 'uuid'

const filter = createFilterOptions()

export default publisher(connect('skills')(({ skills, selectedSkills, onSkillSelected, renderSkill, publish }) => {
  const filterOptions = (options, params) => (
    [...filter(options, params), ...(
      !params.inputValue ? [] : [{
        newSkillName: params.inputValue,
        name: `Add "${params.inputValue}"`,
      }]
    )]
  )

   const handleChange = (e, value, reason) => {
    if(reason === 'select-option') {
      if(value.newSkillName) {
        const newSkill = { skillId: uuid(), name: value.newSkillName }
        publish('skill', newSkill)
        onSkillSelected(newSkill)
      } else {
        onSkillSelected(value)
      }
    }
  }

  const availableSkills = skills.filter(({ skillId }) => selectedSkills.every(x => x.skillId !== skillId))

  return (
    <ColumnBox className="skills">
      <Autocomplete
        freeSolo
        selectOnFocus
        clearOnBlur
        handleHomeEndKeys
        options={availableSkills}
        renderInput={params =>
          <TextField {...params} label="Select skill" variant="outlined" />
        }
        renderOption={option => option.name}
        getOptionLabel={option => option.newSkillName ? '' : (option.name || '')}
        filterOptions={filterOptions}
        onChange={handleChange}
      />
      {renderSkill && <div>
        {selectedSkills.map(skill => (
          <Fragment key={skill.skillId}>{renderSkill(skill)}</Fragment>
        ))}
      </div>}
    </ColumnBox>
  )
}))
