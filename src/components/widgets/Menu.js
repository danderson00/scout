import React from 'react'
import { IconButton, Menu, MenuItem } from '@material-ui/core'
import MoreVertIcon from '@material-ui/icons/MoreVert'

export default ({ className, items }) => {
  const [anchorElement, setAnchorElement] = React.useState(null);

  const open = e => setAnchorElement(e.currentTarget)
  const close = () => setAnchorElement(null)

  const handleSelect = item => {
    close()
    item.handler()
  }

  return (
    <div className={className}>
      <IconButton onClick={stopPropagation(open)}>
        <MoreVertIcon />
      </IconButton>
      <Menu
        anchorEl={anchorElement}
        keepMounted
        open={Boolean(anchorElement)}
        onClose={stopPropagation(close)}
      >
        {items.map(item => (
          <MenuItem
            key={item.text}
            onClick={stopPropagation(() => handleSelect(item))}
          >
            {item.text}
          </MenuItem>
        ))}
      </Menu>
    </div>
  )
}

// if there are elements with click handlers underneath the menu, this stops the event propagating
const stopPropagation = handler => e => {
  e && e.stopPropagation()
  handler(e)
}
