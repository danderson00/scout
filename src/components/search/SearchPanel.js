import React from 'react'
import { publisher, auth } from '@x/unify.react'
import { Box, InputAdornment } from '@material-ui/core'
import { RowBox } from '../widgets'
import SearchIcon from '@material-ui/icons/Search'
import { Form, Text } from '@danderson00/react-forms-material'
import './SearchPanel.scss'

export default auth(publisher(({ query, publish, onSearch, auth }) => (
  <RowBox className="search-panel" alignItems="center" justifyContent="space-between" p={5}>
    <Box mr={6} flexGrow={1} pr={8}>
      <Form onSubmit={throttle(({ query }) => {
        publish('search', { query })
        if(onSearch) {
          onSearch(query)
        }
      })}>
        <Text
          name="query"
          value={query}
          submitOnChange
          variant="outlined"
          fullWidth
          margin="none"
          placeholder="Search..."
          endAdornment={
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          }
        />
      </Form>
    </Box>
    <Box px={4}><a href="about">About</a></Box>
    <Box px={4}><a href="help">Help & Support</a></Box>
    <Box px={4}>
      <button
        className="link"
        onClick={() => auth.logout().then(() => publish('loggedOut'))}
      >
        Log out
      </button>
    </Box>
  </RowBox>
)))

// I tried to put this in react-forms but get react refs being null after a setTimeout...
const throttle = (target, delay = 200) => {
  let timeout

  return (...args) => {
    clearTimeout(timeout)
    timeout = setTimeout(
      () => target(...args),
      delay
    )
  }
}