import React from 'react'
import { publisher } from '@x/unify.react'
import { v4 as uuid } from 'uuid'
import { Card, CardContent, Box } from '@material-ui/core'
import { ColumnBox, RowBox, Star, Check, Menu } from '../widgets'
import avatar from '../../images/search-result-avatar.png'
import './SearchResult.scss'

export default publisher(({ contractor: { contractorId, name, role, rate, averageRating }, publish }) => (
  <Card
    square
    elevation={0}
    className="clickable search-result"
    onClick={() => publish('contractorSelected', { contractorId })}
  >
    <CardContent>
      <Menu
        className="menu-trigger"
        items={[
          { text: 'Compare', handler: () => publish('compare', { comparisonId: uuid(), contractorId }) }
        ]}
      />

      <ColumnBox alignItems="center">
        <img src={avatar} alt={name} />
        <h3 className="serif">{name}</h3>
        <h5 className="grey-color">{role}</h5>
      </ColumnBox>

      <RowBox alignItems="center" justifyContent="space-between">
        <Box flexGrow={1}>Scout rate</Box>
        <Box>${rate || <span className="grey-color">N/A</span>}</Box>
      </RowBox>

      <RowBox alignItems="center" justifyContent="space-between">
        <Box flexGrow={1}>Scout score</Box>
        <Box><Star size={14} />{(averageRating || 0).toFixed(1)}</Box>
      </RowBox>

      <RowBox alignItems="center" justifyContent="space-between">
        <Box flexGrow={1}>Scout verified</Box>
        <Box><Check size={14} />80%</Box>
      </RowBox>
    </CardContent>
  </Card>
))
