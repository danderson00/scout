import React from 'react'
import { Grid, Box, Button, Chip } from '@material-ui/core'
import { RowBox, Insights, DialogButton } from '../widgets'
import SearchResult from './SearchResult'
import AddContractor from '../AddContractor'
import FormatListBulletedIcon from '@material-ui/icons/FormatListBulleted'
import ViewModuleIcon from '@material-ui/icons/ViewModule'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import AddIcon from '@material-ui/icons/AddCircle'
import './SearchResults.scss'

export default ({ results }) => (
  <Box className="search-results">
    <Grid container>
      <Grid item xs={12} className="search-results-header">
        <RowBox justifyContent="space-between">
          <Box flexGrow={1}>
            A total of <b>{results.length}</b> results
          </Box>
          <Box>
            <Button variant="outlined">
              <Insights />
              See Project Manager insights
            </Button>
          </Box>
        </RowBox>

        <RowBox alignItems="center" justifyContent="space-between">
          <Box flexGrow={1}>
            <Chip label="Project manager" />
            <Chip label="Available now" />
          </Box>
          <RowBox alignItems="center">
            <FormatListBulletedIcon />
            <ViewModuleIcon />
            <span>Sort by: <b>Relevance</b></span>
            <KeyboardArrowDownIcon />
          </RowBox>
        </RowBox>
      </Grid>

      {results.map(contractor => (
        <Grid item key={contractor.contractorId}>
          <SearchResult contractor={contractor} />
        </Grid>
      ))}

      <div><DialogButton component={AddContractor} icon>
        <AddIcon style={{ fontSize: 48 }} color="secondary" />
      </DialogButton></div>
    </Grid>
  </Box>
)
