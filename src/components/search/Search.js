import React from 'react'
import { useEvaluate } from '@x/unify.react'
import { Box } from '@material-ui/core'
import { ColumnBox, RowBox } from '../widgets'
import Layout from '../Layout'
import SearchPanel from './SearchPanel'
import SearchResults from './SearchResults'
import roles from '../../images/search-roles.png'
import footer from '../../images/search-footer.png'
import './Search.scss'

export default ({ query }) => {
  const [results, search] = useEvaluate('search', {}, [query || ''])

  return (
    <Layout activeItem="Scout">
      <ColumnBox className="search">
        <SearchPanel query={query} onSearch={search} />

        <RowBox>
          <Box flexGrow={1}>
            {results
              ? <SearchResults results={results} />
              : <p>Enter a search query</p>
            }
          </Box>
          <Box>
            <img src={roles} alt="roles" />
          </Box>
        </RowBox>

        <Box>
          <img src={footer} alt="footer" className="search-footer" />
        </Box>
      </ColumnBox>
    </Layout>
  )
}
