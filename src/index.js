import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { Provider } from '@x/unify.react'
import consumer from '@x/unify'
import { v4 as uuid } from 'uuid'

window.clientId = localStorage['clientId'] = localStorage['clientId'] || uuid()

const url = window.location.hostname.includes('localhost')
  ? 'ws://localhost:3001'
  : `${window.location.protocol === 'http:' ? 'ws' : 'wss'}://${window.location.host}/`

const theme = createMuiTheme({
  spacing: 4,
  palette: {
    secondary: { main: '#ee4743' }
  }
})

consumer({ url, log: { unhandled: true } }).connect().then(host =>
  ReactDOM.render(
    <Provider host={host}>
      <MuiThemeProvider theme={theme}>
        <App />
      </MuiThemeProvider>
    </Provider>,
    document.getElementById('root')
  )
)
