module.exports = ({ userScope }) => ({
  contractor: o => o.assign({
    contractorId: o => o.topic('contractor').select('contractorId'),
    '...details': o => o.topic('contractor').accumulate('contractor'),
    averageRating: o => o.topic('review').average('rating')
  }),
  contractors: o => o.groupBy('contractorId', o => o.contractor()),
  reviews: o => o.groupBy('reviewId', o => o.topic('review').accumulate()),
  projects: o => o.groupBy('projectId', o => o.topic('project').accumulate()),
  employment: o => o.groupBy('employmentId', o => o.topic('employment').accumulate()),

  selectedContractors: o => o
    .topic('contractorListed')
    .groupBy(
      'contractorId',
      undefined,
      o => o.where(x => !x.status)
    ),

  recruiter: o => o.assign({
    recruiterId: o => o.topic('recruiter').select('recruiterId'),
    '...details': o => o.topic('recruiter').accumulate('recruiter')
  }),

  skills: o => o.topic('skill').groupBy('skillId', o => o.accumulate()),
  selectedSkills: o => o.topic('skillSelected').groupBy('skillId',
    o => o.assign({
      skillId: o => o.select('skillId'),
      name: o => o.join('skillId', o => o.topic('skill').select('name'))
    }),
    o => o.map(x => x.removed)
  ),

  openings: o => o.groupBy('openingId', o => o.opening()),
  opening: o => o.assign({
    openingId: o => o.topic('opening').select('openingId'),
    '...details': o => o.topic('opening').accumulate('opening')
  }),
  matchingContractors: (o, skillIds) =>
    o.groupBy('contractorId',
      (o, contractorId) => o
        .topic('skillSelected')
        .groupBy('skillId', o => o.select('skillId'), o => o.map(x => x.removed))
        .mapAll(contractorSkillIds => ({ contractorSkillIds, contractorId }))
    )
    .map(({ contractorId, contractorSkillIds = [] } = {}) => ({
      contractorId,
      contractorSkillIds,
      skillIds,
      weight: skillIds.reduce(
        (weight, skillId) => weight + (contractorSkillIds.includes(skillId) ? 1 : 0),
        0
      )
    }))
    .where(({ weight }) => weight > 0)
    .orderByDescending('weight'),

  search: (o, query = '') => o
    .groupBy('contractorId', o => o.contractor())
    .where(x => x.name && x.name.toLowerCase().includes(query.toLowerCase())),

  user: [userScope,
    o => o.assign({
      type: o => o.topic('user.type').select('type'),
      contractorId: o => o.topic('user.contractor').select('contractorId'),
      contractor: o => o.topic('user.contractor').join('contractorId', o => o.contractor()),
      recruiterId: o => o.topic('user.recruiter').select('recruiterId'),
      recruiter: o => o.topic('user.recruiter').join('recruiterId', o => o.recruiter())
    })
  ]
})
