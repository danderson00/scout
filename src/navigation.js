import { Machine } from '@x/unify.react'

const signup = {
  initial: 'SelectType',
  onDone: 'dashboard/Dashboard',
  states: {
    'SelectType': {
      on: {
        'user.type': [
          {
            target: 'Contractor',
            cond: (_, { message }) => message.type === 'contractor'
          },
          {
            target: 'Recruiter',
            cond: (_, { message }) => message.type === 'recruiter'
          }
        ]
      }
    },
    'Contractor': { on: { 'contractor': 'Done' } },
    'Recruiter': { on: { 'recruiter': 'Done' } },
    'Done': { type: 'final' }
  }
}

export default Machine({
  initial: 'account/Login',
  on: {
    'dashboard': 'dashboard/Dashboard',
    'loggedOut': 'account/Login',
    'search': 'search/Search',
    'scout': 'search/Search',
    'recruit': 'recruit/Recruit',
    'compare': 'compare/Compare'
  },
  states: {
    'account/Login': {
      on: {
        'loggedIn': [
          {
            target: 'dashboard/Dashboard',
            cond: ({ modelObservable }) => Boolean(modelObservable().user.contractorId || modelObservable().user.recruiterId)
          },
          {
            target: 'signup',
          }
        ]
      }
    },
    'dashboard/Dashboard': {
      on: {
        'createOpening': 'recruit/CreateOpening',
        'openingSelected': 'recruit/Opening'
      }
    },
    'search/Search': {
      meta: { props: ['query'] },
      on: { 'contractorSelected': 'profile/Contractor' }
    },
    'profile/Contractor': {
      meta: { scope: ['contractorId'] },
    },
    'recruit/CreateOpening': {
      on: { 'opening': 'recruit/Opening' }
    },
    'recruit/Recruit': {
      on: { 'openingSelected': 'recruit/Opening' }
    },
    'recruit/Opening': {
      meta: { scope: ['openingId'] },
      on: { 'contractorSelected': 'profile/Contractor' }
    },
    'compare/Compare': {
      meta: { scope: ['comparisonId'] },
      on: { 'addComparison': 'compare/Add' }
    },
    'compare/Add': {
      meta: { scope: ['comparisonId'] }
    },
    signup
  }
})