module.exports = ({ required, scopeOwnerOnly, userId, uuid }) => ({
  'user.type': {
    userId,
    type: String
  },
  'user.contractor': {
    userId,
    contractorId: uuid
  },
  'user.recruiter': {
    userId,
    recruiterId: uuid
  },
  contractor: [scopeOwnerOnly('contractorId'), {
    contractorId: [required, String],
    contractor: {
      name: [required, String],
      role: String,
      employer: String,
      rate: Number
    }
  }]
})