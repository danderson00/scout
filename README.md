# scout

An `Unify` sample project.

## Installation

```shell
git clone https://gitlab.com/danderson00/scout.git
cd scout
yarn # or `npm install`
```

## Usage

```shell
yarn start # or npm start
```

This will open your default browser to the landing page of the application.
