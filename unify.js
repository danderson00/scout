module.exports = {
  scopes: ['contractorId', 'reviewId', 'comparisonId', 'userId', 'recruiterId', 'openingId', 'skillId'],
  storage: { client: 'sqlite3', connection: { filename: __dirname + '/scout.db' } },
  auth: { secret: 'scout', password: true }
}