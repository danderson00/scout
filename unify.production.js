module.exports = {
  port: 80,
  requireSsl: false,
  allowRawQueries: true,
  strictApi: false,
  strictTypes: false,
  scopes: ['contractorId', 'reviewId', 'comparisonId', 'userId', 'recruiterId', 'openingId', 'skillId'],
  storage: { client: 'sqlite3', connection: { filename: '/home/dale/scout.db' } },
  auth: { secret: 'scout', password: true }
}